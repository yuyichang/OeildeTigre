package tiger.oeildetigre;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;

import org.opencv.android.JavaCameraView;

/**
 * Created by tiger on 2017/5/25.
 */

public class CameraMode extends JavaCameraView {
    private static final String TAG = "Camera mode";
    Camera camera = mCamera;

    public CameraMode(Context context, int cameraId) {
        super(context, cameraId);
    }

    public void setCameraMode() {
        if(camera != null) {
            Camera.Parameters params= camera.getParameters();

            Log.i(TAG, "Exposure Compensation: " + params.getExposureCompensation());
            //params.setWhiteBalance();
            //camera.setParameters(params);
        }
    }
}
